/**
 * @file example2.c
 * @author Matías S. Ávalos (msavalos@gmail.com)
 * @brief Long data lenght
 * @version 0.4.8
 * @date 2021-02-08
 * 
 * This example tries to send a very long data. If the buffer has enough space,
 * it sends it all at once. If not, it sends it by filling the buffer byte by 
 * byte and putting a new one at the same time that it is freed.
 * 
 * @copyright Copyright (c) 2020-2021
 * 
 * This file is part of Serial lib API for libOpenCM3
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <string.h>
#include <serial.h>

int main(void)
{
    const char *msg =
        "This program is free software: you can redistribute it and/or modify "
        "it under the terms of the GNU Lesser General Public License as "
        "published by the Free Software Foundation, either version 3 of the "
        "License, or (at your option) any later version.\r\n";

    const uint32_t msglen = strlen(msg);

    // Setup the system clock
    rcc_clock_setup_in_hse_8mhz_out_72mhz();

    // PC13 (LED) as output:
    rcc_periph_clock_enable(RCC_GPIOC);
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
    gpio_set(GPIOC, GPIO13);

    // USART2 8,N,1 115200bps
    serial_begin(USART2, BAUD115K2);

    gpio_clear(GPIOC, GPIO13); // start buffering.

    if (serial_sendable(USART2) > msglen) // has enough space in the buffer?
        serial_puts(USART2, msg);         // send all at once
    else                                  // if not:
    {
        uint32_t i = 0;
        while (i != msglen)                   // all masage?
            if (serial_write(USART2, msg[i])) // it fits?
                i++;                          // next byte...
    }
    gpio_set(GPIOC, GPIO13); // end of buffering.

    while (true)
        ; // halt
}
