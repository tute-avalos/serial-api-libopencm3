/**
 * @file serial.h
 * @author Matías S. Ávalos (msavalos@gmail.com)
 * @brief Definición de funciones para la abstracción de la USART en la BluePill utilizando libOpenCM3
 * @version 0.4.9
 * @date 2022-02-09
 * 
 * @copyright Copyright (c) 2020-2021
 * 
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#ifndef __SERIAL_H__
#define __SERIAL_H__

#include <libopencm3/stm32/usart.h>

#define USE_USART1 (1U << 0) ///< @brief Se utiliza la USART1
#define USE_USART2 (1U << 1) ///< @brief Se utiliza la USART2
#define USE_USART3 (1U << 2) ///< @brief Se utiliza la USART3

/** @brief Se utilizan todas las USARTs*/
#define USE_ALL_USARTS (USE_USART1 | USE_USART2 | USE_USART3)

/**
 * @brief Se determina cuales USARTs se van a utilizar.
 * 
 * Se utiliza para determinar las USARTs que necesita la aplicación especifica
 * para declarar los buffers asociados a cada una de estas con la intención
 * de salvar espacio de datos y evitar declarar buffers que no van a ser 
 * utilizados.
 *  
 * Un ejemplo de eso sería:
 * 
 * @code #define USART_USING (USE_USART1|USE_USART3) 
 * 
 */
#ifndef USART_USING
#define USART_USING (USE_ALL_USARTS)
#endif // USART_USING

#if (!(USART_USING & USE_USART1) && !(USART_USING & USE_USART2) && !(USART_USING & USE_USART3))
#error You must use at last one USART.
#else

#if (USART_USING & USE_USART1)
#ifndef MAX_BUFFER_TX1
#define MAX_BUFFER_TX1 128UL ///< @brief Tamaño del buffer de transmisión de la USART1.
#endif // MAX_BUFFER_TX1
#ifndef MAX_BUFFER_RX1
#define MAX_BUFFER_RX1 128UL ///< @brief Tamaño del buffer de recepción de la USART1.
#endif // MAX_BUFFER_RX1

#if MAX_BUFFER_TX1 < 2
#error MAX_BUFFER_TX1 is too small.  It must be larger than 1.
#elif ((MAX_BUFFER_TX1 & (MAX_BUFFER_TX1 - 1)) != 0)
#error MAX_BUFFER_TX1 must be a power of 2.
#endif

#if MAX_BUFFER_RX1 < 2
#error MAX_BUFFER_RX1 is too small.  It must be larger than 1.
#elif ((MAX_BUFFER_RX1 & (MAX_BUFFER_RX1 - 1)) != 0)
#error MAX_BUFFER_RX1 must be a power of 2.
#endif

#endif // (USART_USING & USE_USART1)
#if (USART_USING & USE_USART2)
#ifndef MAX_BUFFER_TX2
#define MAX_BUFFER_TX2 128UL ///< @brief Tamaño del buffer de transmisión de la USART2.
#endif // MAX_BUFFER_TX2
#ifndef MAX_BUFFER_RX2
#define MAX_BUFFER_RX2 128UL ///< @brief Tamaño del buffer de recepción de la USART2.
#endif // MAX_BUFFER_RX1

#if MAX_BUFFER_TX2 < 2
#error MAX_BUFFER_TX2 is too small.  It must be larger than 1.
#elif ((MAX_BUFFER_TX2 & (MAX_BUFFER_TX2 - 1)) != 0)
#error MAX_BUFFER_TX2 must be a power of 2.
#endif

#if MAX_BUFFER_RX2 < 2
#error MAX_BUFFER_RX2 is too small.  It must be larger than 1.
#elif ((MAX_BUFFER_RX2 & (MAX_BUFFER_RX2 - 1)) != 0)
#error MAX_BUFFER_RX2 must be a power of 2.
#endif

#endif // (USART_USING & USE_USART2)
#if (USART_USING & USE_USART3)
#ifndef MAX_BUFFER_TX3
#define MAX_BUFFER_TX3 128UL ///< @brief Tamaño del buffer de transmisión de la USART3.
#endif // MAX_BUFFER_TX3
#ifndef MAX_BUFFER_RX3
#define MAX_BUFFER_RX3 128UL ///< @brief Tamaño del buffer de recepción de la USART3.
#endif // MAX_BUFFER_RX3

#if MAX_BUFFER_TX3 < 2
#error MAX_BUFFER_TX3 is too small.  It must be larger than 1.
#elif ((MAX_BUFFER_TX3 & (MAX_BUFFER_TX3 - 1)) != 0)
#error MAX_BUFFER_TX3 must be a power of 2.
#endif

#if MAX_BUFFER_RX3 < 2
#error MAX_BUFFER_RX3 is too small.  It must be larger than 1.
#elif ((MAX_BUFFER_RX3 & (MAX_BUFFER_RX3 - 1)) != 0)
#error MAX_BUFFER_RX3 must be a power of 2.
#endif

#endif // (USART_USING & USE_USART3)
#endif // (USART_USING == 0)

/**
 * @brief Velocidades estándar de transmisión y recepción.
 * 
 *  Para no tener que recordar los valores estándar.
 * 
 */
typedef enum
{
    BAUD300 = 300,
    BAUD600 = 600,
    BAUD1200 = 1200,
    BAUD2400 = 2400,
    BAUD4800 = 4800,
    BAUD9600 = 9600,
    BAUD19K2 = 19200,
    BAUD38K4 = 38400,
    BAUD57K6 = 57600,
    BAUD115K2 = 115200,
    BAUD230K4 = 230400,
    BAUD460K8 = 460800,
    BAUD576K = 576000,
    BAUD921K6 = 921600,
    BAUD1M = 1000000,
    BAUD2M = 2000000
} baud_t;

BEGIN_DECLS

/**
 * @brief Inicializa la USART pasada en el argumento usartx con el baudrate indicado.
 * 
 * La inicialización del "puerto serie" es de 8 bits con 1 bit de start sin paridad
 * y sin control de flujo (el cual está disponible para algunas USARTs). Se habilitan
 * las interrupciones para recibir y enviar los datos los cuales se cargan y descargan
 * de buffers circulares.     
 * 
 * @param usartx USART a utilizar @ref usart_reg_base
 * @param baudrate Cantidad de bits por segundo.
 */
void serial_begin(const uint32_t usartx, const baud_t baudrate);

/**
 * @brief Verifica si hay datos en el buffer de recepción.
 * 
 * Se verifica cuántos datos hay en el buffer de rececpción, debe utilizarse antes de
 * intentar hacer un serial_read(), de lo contrario se leera un dato erroneo.
 * 
 * @param usartx USART de la que se consulta el estado de buffer de recepción.
 * @return uint16_t Cantidad de datos disponibles en el buffer de recepción.
 */
uint16_t serial_available(const uint32_t usartx);

/**
 * @brief Se lee el primer dato que esté disponible del buffer de recepción.
 * 
 * @param usartx USART de la cual se lee el buffer de recepción.
 * @return uint8_t Dato extraido del buffer.
 */
uint8_t serial_read(const uint32_t usartx);

/**
 * @brief Indica la cantidad de espacio disponible en el buffer de transmisión.
 * 
 * @param usartx USART de la cual se consulta.
 * @return uint16_t cantidad de bytes disponibles para el envio de datos.
 */
uint16_t serial_sendable(const uint32_t usartx);

/**
 * @brief Se escribe un dato en le buffer de transmisión de la USART.
 * 
 * Se escribe un dato en el buffer, si no había datos pendientes en cola esta función
 * inicializa la transmisión de datos.
 * 
 * @param usartx USART por la cual se van a transmitir los datos.
 * @param data Dato a transmitir.
 * @return true Si se pudo insertar el dato en el buffer.
 * @return false Si el buffer estaba lleno.
 */
bool serial_write(const uint32_t usartx, const uint8_t data);

/**
 * @brief Carga una string en el buffer de transmisión para enviar por la USART.
 * 
 * @param usartx USART por la cual se va a transmitir la cadena.
 * @param str Cadena a transmitir por la USART.
 * @return uint16_t Cantidad de caracteres que pudieron ser cargados en el buffer.
 */
uint16_t serial_puts(const uint32_t usartx, const char *str);

/**
 * @brief Transmite un dato que consta de varios bytes (como una estructura)
 * 
 * @param usartx USART por la cual se va a transmitir los datos.
 * @param data Dirección de memoria del dato a transmitir
 * @param size Tamaño en bytes del dato a transmitir
 * @return uint16_t Cantidad de bytes que entraron en el buffer de transmisión.
 */
uint16_t serial_send_data(const uint32_t usartx, const void *data, uint32_t size);

END_DECLS

#endif // __SERIAL_H__

