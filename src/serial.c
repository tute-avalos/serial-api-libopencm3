/**
 * @file serial.c
 * @author Matías S. Ávalos (msavalos@gmail.com)
 * @brief Implementación de funciones para la abstracción de la USART en la BluePill utilizando libOpenCM3
 * @version 0.4.9
 * @date 2022-02-09
 * 
 * @copyright Copyright (c) 2020-2021
 * 
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/nvic.h>
#include "serial.h"

/**
 * @brief Estructura que contiene a los buffers y sus índices.
 * 
 */
typedef struct __packet
{
    uint16_t in;
    uint16_t out;
    volatile uint8_t *data;
    uint16_t mask;
} circular_buffer_t;

//Buffers -------------------------------------------------------
#if (USART_USING & USE_USART1)
volatile uint8_t buffer_tx1[MAX_BUFFER_TX1]; ///< @brief Buffer de Transmision.
volatile uint8_t buffer_rx1[MAX_BUFFER_RX1]; ///< @brief Buffer de Recepcion.
/**
 * @brief Estructura que contiene los índices y puntero al buffer de transmisión de la USART1.
 */
static circular_buffer_t cbuff_tx1 = {0, 0, buffer_tx1, MAX_BUFFER_TX1 - 1};
/**
 * @brief Estructura que contiene los índices y puntero al buffer de recepción de la USART1.
 */
static circular_buffer_t cbuff_rx1 = {0, 0, buffer_rx1, MAX_BUFFER_RX1 - 1};
#endif
#if (USART_USING & USE_USART2)
volatile uint8_t buffer_tx2[MAX_BUFFER_TX2]; ///< @brief Buffer de Transmision.
volatile uint8_t buffer_rx2[MAX_BUFFER_RX2]; ///< @brief Buffer de Recepcion.
/**
 * @brief Estructura que contiene los índices y puntero al buffer de transmisión de la USART2.
 */
static circular_buffer_t cbuff_tx2 = {0, 0, buffer_tx2, MAX_BUFFER_TX2 - 1};
/**
 * @brief Estructura que contiene los índices y puntero al buffer de recepción de la USART2.
 */
static circular_buffer_t cbuff_rx2 = {0, 0, buffer_rx2, MAX_BUFFER_RX2 - 1};
#endif
#if (USART_USING & USE_USART3)
volatile uint8_t buffer_tx3[MAX_BUFFER_TX3]; ///< @brief Buffer de Transmision.
volatile uint8_t buffer_rx3[MAX_BUFFER_RX3]; ///< @brief Buffer de Recepcion.
/**
 * @brief Estructura que contiene los índices y puntero al buffer de transmisión de la USART3.
 */
static circular_buffer_t cbuff_tx3 = {0, 0, buffer_tx3, MAX_BUFFER_TX3 - 1};
/**
 * @brief Estructura que contiene los índices y puntero al buffer de recepción de la USART3.
 */
static circular_buffer_t cbuff_rx3 = {0, 0, buffer_rx3, MAX_BUFFER_RX3 - 1};
#endif
//---------------------------------------------------------------

//Funciones privadas para el manejo de buffers ------------------
/**
 * @brief Inserta un dato en un buffer.
 * 
 * @param buff Buffer en el cual insertar el dato.
 * @param data El dato a insertar.
 * @return true Si había espacio para insertar el dato.
 * @return false Si no se pudo insertar el dato en el buffer
 */
static bool push(circular_buffer_t *buff, const uint8_t data);

/**
 * @brief Saca el primer dato disponible del buffer.
 * 
 * @param buff Buffer del cual sacar el dato.
 * @return int16_t (-1) si no había datos en el buffer
 * @return int16_t castear a (uint8_t) para obtener el dato del buffer.
 */
static int16_t pop(circular_buffer_t *buff);
//---------------------------------------------------------------

void serial_begin(const uint32_t usartx, const baud_t baudrate)
{
    uint8_t usart_irq = 0;
    enum rcc_periph_clken rcc_usart = 0;
#if (USART_USING & USE_USART1)
    if (usartx == USART1)
    {
        rcc_usart = RCC_USART1;
        usart_irq = NVIC_USART1_IRQ;
        if (AFIO_MAPR & AFIO_MAPR_USART1_REMAP)
        { // si está remapeada la USART1:
            rcc_periph_clock_enable(RCC_GPIOB);
            gpio_set_mode(
                GPIOB,
                GPIO_MODE_OUTPUT_10_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
                GPIO_USART1_RE_TX);
            gpio_set_mode(
                GPIOB,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                GPIO_USART1_RE_RX);
        }
        else // si no está remapeada la USART1:
        {
            rcc_periph_clock_enable(RCC_GPIOA);
            gpio_set_mode(
                GPIOA,
                GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
                GPIO_USART1_TX);
            gpio_set_mode(
                GPIOA,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                GPIO_USART1_RX);
        }
    }
#endif //  --- (USART_USING & USE_USART1) ---
#if (USART_USING & USE_USART2)
    if (usartx == USART2)
    {
        rcc_usart = RCC_USART2;
        usart_irq = NVIC_USART2_IRQ;
        rcc_periph_clock_enable(RCC_GPIOA);
        gpio_set_mode(
            GPIOA,
            GPIO_MODE_OUTPUT_10_MHZ,
            GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
            GPIO_USART2_TX);
        gpio_set_mode(
            GPIOA,
            GPIO_MODE_INPUT,
            GPIO_CNF_INPUT_FLOAT,
            GPIO_USART2_RX);
    }
#endif // --- (USART_USING & USE_USART2) ---
#if (USART_USING & USE_USART3)
    if (usartx == USART3)
    {
        rcc_usart = RCC_USART3;
        usart_irq = NVIC_USART3_IRQ;
        rcc_periph_clock_enable(RCC_GPIOA);
        gpio_set_mode(
            GPIOB,
            GPIO_MODE_OUTPUT_10_MHZ,
            GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
            GPIO_USART3_TX);
        gpio_set_mode(
            GPIOB,
            GPIO_MODE_INPUT,
            GPIO_CNF_INPUT_FLOAT,
            GPIO_USART3_RX);
    }
#endif // --- (USART_USING & USE_USART3) ---
    if (rcc_usart != 0)
    {
        rcc_periph_clock_enable(rcc_usart);

        usart_set_databits(usartx, 8);
        usart_set_mode(usartx, USART_MODE_TX_RX);
        usart_set_parity(usartx, USART_PARITY_NONE);
        usart_set_stopbits(usartx, USART_STOPBITS_1);
        usart_set_flow_control(usartx, USART_FLOWCONTROL_NONE);
        usart_set_baudrate(usartx, (uint32_t)baudrate);
        usart_enable_rx_interrupt(usartx);
        usart_enable(usartx);

        nvic_enable_irq(usart_irq);
    }
}

uint16_t serial_available(const uint32_t usartx)
{
#if (USART_USING & USE_USART1)
    if (usartx == USART1)
        return (cbuff_rx1.in - cbuff_rx1.out);
#endif
#if (USART_USING & USE_USART2)
    if (usartx == USART2)
        return (cbuff_rx2.in - cbuff_rx2.out);
#endif
#if (USART_USING & USE_USART3)
    if (usartx == USART3)
        return (cbuff_rx3.in - cbuff_rx3.out);
#endif
    return 0U;
}

uint8_t serial_read(const uint32_t usartx)
{
#if (USART_USING & USE_USART1)
    if (usartx == USART1)
        return (uint8_t)pop(&cbuff_rx1);
#endif
#if (USART_USING & USE_USART2)
    if (usartx == USART2)
        return (uint8_t)pop(&cbuff_rx2);
#endif
#if (USART_USING & USE_USART3)
    if (usartx == USART3)
        return (uint8_t)pop(&cbuff_rx3);
#endif
    return 0;
}

uint16_t serial_sendable(uint32_t usartx)
{
#if (USART_USING & USE_USART1)
    if (usartx == USART1)
        return (cbuff_tx1.mask - (cbuff_tx1.in - cbuff_tx1.out)) + 1U;
#endif
#if (USART_USING & USE_USART2)
    if (usartx == USART2)
        return (cbuff_tx2.mask - (cbuff_tx2.in - cbuff_tx2.out)) + 1U;
#endif
#if (USART_USING & USE_USART3)
    if (usartx == USART3)
        return (cbuff_tx3.mask - (cbuff_tx3.in - cbuff_tx3.out)) + 1U;
#endif
    return 0U;
}

bool serial_write(const uint32_t usartx, const uint8_t data)
{
    bool result = false;
#if (USART_USING & USE_USART1)
    if (usartx == USART1)
    {
        result = push(&cbuff_tx1, data);
        if ((uint16_t)(cbuff_tx1.in - cbuff_tx1.out) == 1)
            usart_enable_tx_interrupt(usartx);
    }
#endif
#if (USART_USING & USE_USART2)
    if (usartx == USART2)
    {
        result = push(&cbuff_tx2, data);
        if ((uint16_t)(cbuff_tx2.in - cbuff_tx2.out) == 1)
            usart_enable_tx_interrupt(usartx);
    }
#endif
#if (USART_USING & USE_USART3)
    if (usartx == USART3)
    {
        result = push(&cbuff_tx3, data);
        if ((uint16_t)(cbuff_tx3.in - cbuff_tx3.out) == 1)
            usart_enable_tx_interrupt(usartx);
    }
#endif
    return result;
}

uint16_t serial_puts(const uint32_t usartx, const char *str)
{
    uint32_t nsend = 0;

    while (*str && serial_write(usartx, *str++))
        nsend++;

    return nsend;
}

uint16_t serial_send_data(const uint32_t usartx, const void *data, uint32_t size)
{
    uint32_t nsend = 0;
    uint8_t *ddata = (uint8_t *)data;

    do
    {
        if (!serial_write(usartx, *ddata++))
            break;
        nsend++;
    } while (--size);

    return nsend;
}

static bool push(circular_buffer_t *buff, const uint8_t data)
{
    bool result = false;
    if ((uint16_t)(buff->in - buff->out) < buff->mask)
    {
        result = true;
        buff->data[buff->in & buff->mask] = data;
        buff->in++;
    }
    return result;
}

static int16_t pop(circular_buffer_t *buff)
{
    int16_t ndata = -1;
    if (buff->in != buff->out)
    {
        ndata = (uint16_t)(buff->data[buff->out & buff->mask]);
        buff->out++;
    }
    return ndata;
}

#if (USART_USING & USE_USART1)
/**
 * @brief Rutina de interrupción de la USART1
 * 
 * Transmisión:
 * En esta rutina se saca del buffer de transmisión el primer dato y se coloca
 * en el registro de datos de la USART1.
 * 
 * Recepción:
 * En esta rutina se pone el dato recibido en el registro de datos de la USART1
 * en la primera posición libre del buffer de recepción.
 * 
 */
void usart1_isr(void)
{
    if (usart_get_flag(USART1, USART_SR_TXE))
    {
        int16_t ndata = pop(&cbuff_tx1);
        if (ndata != -1)
        {
            usart_send(USART1, (uint8_t)ndata);

            if (cbuff_tx1.in == cbuff_tx1.out)
                usart_disable_tx_interrupt(USART1);
        }
    }
    if (usart_get_flag(USART1, USART_SR_RXNE))
    {
        uint8_t ndata = (uint8_t)usart_recv(USART1);
        push(&cbuff_rx1, ndata);
    }
}
#endif
#if (USART_USING & USE_USART2)
/**
 * @brief Rutina de interrupción de la USART2
 * 
 * Transmisión:
 * En esta rutina se saca del buffer de transmisión el primer dato y se coloca
 * en el registro de datos de la USART2.
 * 
 * Recepción:
 * En esta rutina se pone el dato recibido en el registro de datos de la USART2
 * en la primera posición libre del buffer de recepción.
 * 
 */
void usart2_isr(void)
{
    if (usart_get_flag(USART2, USART_SR_TXE))
    {
        int16_t ndata = pop(&cbuff_tx2);
        if (ndata != -1)
        {
            usart_send(USART2, (uint8_t)ndata);

            if (cbuff_tx2.in == cbuff_tx2.out)
                usart_disable_tx_interrupt(USART2);
        }
    }
    if (usart_get_flag(USART2, USART_SR_RXNE))
    {
        uint8_t ndata = (uint8_t)usart_recv(USART2);
        push(&cbuff_rx2, ndata);
    }
}
#endif
#if (USART_USING & USE_USART3)
/**
 * @brief Rutina de interrupción de la USART1
 * 
 * Transmisión:
 * En esta rutina se saca del buffer de transmisión el primer dato y se coloca
 * en el registro de datos de la USART3.
 * 
 * Recepción:
 * En esta rutina se pone el dato recibido en el registro de datos de la USART3
 * en la primera posición libre del buffer de recepción.
 * 
 */
void usart3_isr(void)
{
    if (usart_get_flag(USART3, USART_SR_TXE))
    {
        int16_t ndata = pop(&cbuff_tx3);
        if (ndata != -1)
        {
            usart_send(USART3, (uint8_t)ndata);

            if (cbuff_tx3.in == cbuff_tx3.out)
                usart_disable_tx_interrupt(USART3);
        }
    }
    if (usart_get_flag(USART3, USART_SR_RXNE))
    {
        uint8_t ndata = (uint8_t)usart_recv(USART3);
        push(&cbuff_rx3, ndata);
    }
}
#endif

