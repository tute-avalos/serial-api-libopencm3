example1: libopencm3 clean
	make -C src all USING_USART=USE_USART1 CFILES="serial.c ../examples/toupper-tolower/example1.c"

example2: libopencm3 clean
	make -C src all USING_USART=USE_USART2 CFILES="serial.c ../examples/long-data/example2.c"

example3: libopencm3 clean
	make -C src all CFILES="serial.c ../examples/raw-data/example3.c"

libopencm3:
	make -C libopencm3

clean:
	make -C src clean

flash:
	make -C src flash

st-flash:
	make -C src st-flash

.PHONY: libopencm3 example1 example2 example3 clean flash st-flash
