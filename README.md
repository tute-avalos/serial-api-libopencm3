# Serial lib for libOpenCM3

This is a higher level API for STM32F103x with libOpenCM3 and can be use in C projects for interfacing with the USART peripheral. It use a cicular buffer for each USART, but you can save program and data space by setting the `USING` define at `serial.h` and the size of the buffer.

| :exclamation: | The buffer has to be power of 2 (2, 4, 8, 16, etc.), default value is 128 bytes. |
| ------------- | -------------------------------------------------------------------------------- |

## Functions brief description

This was tested on a Blue Pill, the functions are self explained and very _Arduino-like_. They use snake_case to match the libOpenCM3 style of code, all of them starts with `serial_` followed by the functions description:

* `begin()`: Initializes the USARTx with the specified baudrate.
* `available()`: Indicates the amount of data  in the _RX Buffer_.
* `sendable()`: Indicates the free space in the _TX Buffer_.
* `read()`: Reads one byte from the _RX Buffer_.
* `write()`: Puts a byte in the _TX Buffer_, if it's the first byte it initialize the communication.
* `puts()`: Loads a String in the _TX Buffer_.
* `send_data()`: Loads an amount of bytes from a `void` pointer, with the specified size. It's minded for sending raw data like data structures or streams of bytes for custom protocols.

## Clarifications

The USART will be configured as 8 bits of data, no parity, 1 stop bit (as fact standar). You can change this in the implementation of `serial_begin()` function at `serial.c`, libOpenCM3 has a very straightforward API for this purpose.

This is all made non-blocking, using the interruptions of each USART, feel free of made any change you might need for your application, especially i suggest to fix the priority of the interruption.

The documentation is spanish only, that's why I'm making all the clarifications here. Good luck!

## Compiling

I highly recommend you to use [PlatformIO](http://platformio.org/) and add in your `platformio.ini` this:

```bash
lib_deps = Serial-libOpenCM3
```

Otherwise, you will need to have installed and configured the next tools:

* `python` in your path
* `arm-none-eabi-gcc` toolchain
* `openocd` or `stlink-tools`
* `make`

## Selecting the USARTs to use

In PlatformIO you can edit the `platformio.ini` selecting the USART like this:

```bash
build_flags = -DUSART_USING="USE_USART1"
```

If you want to use more than one:

```bash
build_flags = -DUSART_USING="'(USE_USART1|USE_USART3)'"
```

## Change the TX or RX buffer size

In PlatformIO you can edit the `platformio.ini` adding to the `build_flags`:

```bash
build_flags = 
    -DMAX_BUFFER_TX1 256
    -DMAX_BUFFER_RX1 512
```

The values has to be power of 2. Ending with TX1 for USART1, TX2 for USART2, etc.

### Cloning the project

If you want to try the examples without using PlatformIO, you have to clone recursive because it use libOpenCM3 as git submodule:

```git
git clone --recursive  https://gitlab.com/tute-avalos/serial-api-libopencm3.git
```
If you already have cloned the project you can do the follow:

```git
git submodule update --init
```
### Compiling and burn the examples into the Blue Pill

You can `make example1`, `make example2`, etc. like this:

```bash
make example1
make flash    # for openocd
make st-flash # for stlink-tools
```
